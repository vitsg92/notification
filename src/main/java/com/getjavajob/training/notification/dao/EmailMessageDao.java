package com.getjavajob.training.notification.dao;

import com.getjavajob.training.notification.model.EmailMessage;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface EmailMessageDao extends MongoRepository<EmailMessage, String> {

    List<EmailMessage> findAllBySentOrderById(boolean sent);
}
