package com.getjavajob.training.notification.controller;

import com.getjavajob.training.notification.dao.EmailMessageDao;
import com.getjavajob.training.notification.model.EmailMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

import java.util.List;

@EnableScheduling
@Controller
public class EmailController {
    private final JavaMailSender emailSender;
    private final EmailMessageDao emailMessageDao;

    @Autowired
    public EmailController(JavaMailSender emailSender, EmailMessageDao emailMessageDao) {
        this.emailSender = emailSender;
        this.emailMessageDao = emailMessageDao;
    }

    @JmsListener(destination = "email")
    public void receiveMessage(String message) {
        EmailMessage emailMessage = new EmailMessage();
        emailMessage.setFrom("vitsg92.sn.notification@gmail.com");
        emailMessage.setTo("vitsg92@gmail.com");
        emailMessage.setSubject("Произошло новое событие в соц. сети");
        emailMessage.setText(message);
        try {
            emailSender.send(emailMessage);
            emailMessage.setSent(true);
        } catch (RuntimeException e) {
            emailMessage.setSent(false);
        }
        try {
            emailMessageDao.save(emailMessage);
        } catch (RuntimeException e) {
            //...
        }
    }

    @Scheduled(fixedDelay = 5000)
    public void sendNotTransferredJmsMessages() {
        try {
            List<EmailMessage> emailMessages = emailMessageDao.findAllBySentOrderById(false);
            for (EmailMessage emailMessage : emailMessages) {
                emailSender.send(emailMessage);
                emailMessage.setSent(true);
                emailMessageDao.save(emailMessage);
            }
        } catch (RuntimeException e) {
            //...
        }
    }
}
